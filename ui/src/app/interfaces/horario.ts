export interface Horario {
    id: number;
    ponto: {},
    codigo: number;
    dia_semana: string;
    data: Date;
    hora_inicio?: Date;
    hora_termino?: Date;
    obs: string;
    status: string;
}
