import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { map } from 'rxjs/operators';
import { Horario } from '../interfaces/horario';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HorariosService {

  constructor(private http: HttpClient) { }

  buscar(horario: any) : Observable<Horario[]>
  {
    return this.http.get(`${environment.url}/horarios`).pipe(map(res => res as Horario[]));
  }
}
