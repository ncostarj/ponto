import { Component, OnInit } from '@angular/core';

import { HorariosService } from '../../services/horarios.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HorariosService]
})
export class HomeComponent implements OnInit {

  horarios = []

  constructor(private horarioService : HorariosService) {}

  ngOnInit() {
    this.horarioService.buscar({}).subscribe(res => {
      
      res.forEach(function(time) {

          // @TODO fazer os calculos de horarios
          // let horasTrabalhadas = Date();
          // horasTrabalhadas.setTime(time.horasTrabalhadas);
          // let carga_horaria = Date();
          // carga_horaria.setTime('08:48');
          // let entrada = {codigo: time.codigo, data: time.data, hora_inicio: time.hora_inicio, hora_termino: time.hora_termino, horasTrabalhadas: '', status: time.status};

          // console.log(time.horasTrabalhadas);

      });


      this.horarios = res; console.log(res);
    });
  }

}
