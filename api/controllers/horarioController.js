const horarioController = module.exports = {};
const clientMongoDB     = require('../config/databases');

horarioController.all = async (req, res) => {
    try
    {
        const client   = await clientMongoDB.getConnection('mongodb://root:root@192.168.99.100:27017/admin');
        const db       = await clientMongoDB.getDatabase(client, 'estudo');
        const horarios = await db.collection('horarios').find({}).toArray();
        
        res.send(horarios);
    }
    catch(err)
    {
        console.log(err);
    }
}