const express = require('express');
const router = express.Router();
const horariosController = require('./controllers/horarioController');
 
router.get('/', (req, res) => {
  res.send('api works');
});
 
 
// Get Todos
router.get('/horarios', function(req, res, next){

    horariosController.all(req, res);

    // res.send(results);

});
 
module.exports = router;