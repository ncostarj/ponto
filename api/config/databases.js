const MongoClient = require('mongodb').MongoClient;

async function getConnection(server){
    return await MongoClient.connect(server, { useNewUrlParser: true });
}

async function getDatabase(client, databaseName){
    return await client.db(databaseName);
}

module.exports = {
    getConnection,
    getDatabase
}