const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const api = require('./routes');
const app = express();
 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
 
app.use('/api', api); // Set our api routes 
 
const port = process.env.PORT || '3000';  //port setting
app.set('port', port);
app.listen(port, ()=> console.log(`Listening at localhost:${port}`));