@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-md-8 offset-md-2">
        <div class="table-responsive">
            <div class="jumbotron" style="background-color: #eeeeee !important; text-align: center">
                <h2>Sistema de Consulta de Ponto - Empresa</h2>
                <strong>Funcionário: </strong> <span style="font-size: 25px ">{{ $dados->nome }}</span> <br>
                <strong>Empresa: </strong><span>{{ $dados->empresa }}</span> <br>
                <strong>Horário: </strong><span>{{ $dados->inicio }}     </span>/<span>{{ $dados->termino }}     </span> -
                <strong>Intervalo: </strong><span> {{ $dados->intervalo }}</span>
            </div>        
            <table class="table">
                <tr>
                    <td>Código</td>
                    <td>Dia da Semana</td>
                    <td>Data</td>
                    <td>Entrada</td>
                    <td>Saída</td>
                    <td>Horas Trabalhadas</td>
                    <td>Status</td>
                </tr>

                @forelse($dados->horarios as $horario)

                    <tr class="text-center">
                        <td>{{ $horario->codigo }}</td>
                        <td>{{ $horario->dia }}</td>
                        <td>{{ $horario->data->format('d/m/Y') }}</td>
                        <td>{{ substr($horario->entrada,0,5) }}</td>
                        <td>{{ substr($horario->saida,0,5) }}</td>
                        <td>{{ substr($horario->horas_trabalhadas,0,5) }}</td>
                        <td>{{ $horario->observacao }}</td>
                    </tr>

                @empty

                    <tr>
                        <td colspan="7">vazio</td>
                    </tr>

                @endforelse

            </table>
        </div>
    </div>
</div>
@endsection