<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'data'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['codigo','dados_id','dia','data','entrada','saida','horas_trabalhadas','observacao'];    

    public function dados()
    {
        return $this->belongsTo('App\Models\Dados');
    }
}
