<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dados extends Model
{
    //

    public function horarios()
    {
        return $this->hasMany('App\Models\Horario','dados_id');
    }
}
