<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Horario;

class ApiReader extends Controller
{
    //

    public function read()
    {
        $client = new Client(['base_uri' => 'http://localhost:3000/api/']);
        $response = $client->request('GET', 'horarios');
        $body = $response->getBody();
        // Implicitly cast the body to a string and echo it
        $horarios = json_decode($body);

        foreach ($horarios as $horario) {
            print($horario->codigo                   . ' - ' . 
                  $horario->dia_semana               . ' - ' .
                  substr($horario->data, 0, 10)      . ' - ' .
                  substr($horario->hora_inicio, 11, 5)      . ' - ' .
                  substr($horario->hora_termino, 11, 5)         . ' - ' .
                  substr($horario->horas_computadas, 11, 5) . ' - ' .
                  $horario->status                   . "<br/>");

            $h = Horario::create(['codigo'=> $horario->codigo,
                              'dados_id' => 1,
                              'dia' => $horario->dia_semana,
                              'data' => substr($horario->data, 0, 10),
                              'entrada' => substr($horario->hora_inicio, 11, 5),
                              'saida' => substr($horario->hora_termino, 11, 5),
                              'horas_trabalhadas' => substr($horario->horas_computadas, 11, 5),
                              'observacao' => $horario->status]);
        }
    }
}
