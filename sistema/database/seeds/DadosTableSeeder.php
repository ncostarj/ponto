<?php

use Illuminate\Database\Seeder;

class DadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('dados')->insert([
            'empresa' => 'ORION SERVICOS E LOGISTICA LTDA ME',
            'intervalo' => '1 hora',
            'nome' => 'NEWTON GONZAGA COSTA',
            'inicio' => \Carbon\Carbon::createFromFormat('H:i','09:00')->toTimeString(),
            'termino' => \Carbon\Carbon::createFromFormat('H:i','18:48')->toTimeString()
        ]);
    }
}
