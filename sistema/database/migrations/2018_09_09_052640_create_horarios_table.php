<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dados_id');
            $table->integer('codigo');
            $table->string('dia');
            $table->date('data');           
            $table->time('entrada');
            $table->time('saida');
            $table->time('horas_trabalhadas');
            $table->string('observacao');
            $table->foreign('dados_id')->references('id')->on('dados');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
