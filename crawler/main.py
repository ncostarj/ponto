import sys
import time
import datetime
from models.crawler import crawler

def show_message():
    print('Usage python main.py [options]')
    print('\t -s crawl site')
    print('\t -h crawl html')
    print('\t -e export')
    print('\t -i import')    

def main(args):

    if len(args) == 0:
        print('123')
        show_message()
    else:
        spider = crawler()

        if args[0] == '-s':
            spider.set_type('site')
            spider.crawl()
        elif args[0] == '-h':
            spider.set_type('html')
            spider.crawl()
        elif args[0] == '-e':
            spider.export_csv()
        elif args[0] == '-i':
            spider.import_csv() 
        else:
            print('312')
            print(args[0])
            show_message()

main(sys.argv[1:])