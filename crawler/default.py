import sys
import time
import datetime
from models.crawler import crawler

def show_message():
    print('Usage python default.py [options]')
    print('\t -s site')
    print('\t -h html')
    print('\t -e export')    
    print('\t -i import')    

def main(args):

    if len(args) == 0:
        
        show_message()
    
    else:
    
        c = crawler()

        if args[0] == '-s':
            c.set_type('site')
            c.crawl()
        elif args[0] == '-h':
            c.set_type('html')
            c.crawl()
        elif args[0] == '-e':
            c.export_csv()
        elif args[0] == '-i':
            c.import_csv() 
        else:
            show_message()

main(sys.argv[1:])