# -*- coding: utf-8 -*-

import requests
import json
import re 
import sys
from datetime import datetime
from datetime import timedelta
import time
import csv
from bs4 import BeautifulSoup
from pymongo import MongoClient


class ponto():

    def __init__(self):
        self.url     = 'http://ponto.grupoprazo.adv.br/Login/Autenticar/'
        self.usuario = 'ncosta'
        self.senha   = 'prazo@123'

    def crawl(self):
        # Start a session so we can have persistant cookies
        s = requests.session()
        
        # s = requests.session(config={'verbose': sys.stderr})

        login_data = {
            'usuario': self.usuario,
            'senha': self.senha            
        }

        r = s.post(self.url, data=login_data)
        r = s.get('http://ponto.grupoprazo.adv.br/Ponto/ExibirPonto')

        soup = BeautifulSoup(r.text, 'lxml')

        self.extract_info(soup)        

    def crawl_html(self, html):
        soup = BeautifulSoup(html, 'lxml')

        self.extract_info(soup)

    def extract_info(self, soup):
        
        """ Extraindo as informações do ponto """

        print('Início do robot extrator de dados do ponto')

        client         = MongoClient('mongodb://root:root@localhost:27017')
        tabela         = soup.find('div', class_="table-responsive")
        boxFuncionario = tabela.find('div', class_="jumbotron")        
        spans          = boxFuncionario.find_all('span')        
        boxPonto       = tabela.find('table', class_="table")
        horarios       = []

        ponto = dict({
                'nome': spans[0].get_text('', strip=True), 
                'empresa': spans[1].get_text('', strip=True), 
                'inicio': spans[2].get_text('', strip=True), 
                'termino': spans[3].get_text('', strip=True), 
                'intervalo': spans[4].get_text('', strip=True),                
                })

        for linha in boxPonto.find_all('tr', class_="text-center"):

            horario            = linha.find_all('td')            
            data               = horario[2].get_text('', strip=True)            
            hora_inicio        = horario[3].get_text('', strip=True)
            hora_termino       = horario[4].get_text('', strip=True)
            horas_trabalhadas  = horario[5].get_text('', strip=True)
            horas_trabalhadas2 = None
            status             = horario[6].get_text('', strip=True)

            data_formatada        = datetime.strptime(data, '%d/%m/%Y')
            carga_horaria         = datetime.strptime(data + ' 08:48', '%d/%m/%Y %H:%M')
            hora_inicio_formatada = None
            hora_termino_formatada = None
            horas_trabalhadas_formatada = None
            horas_trabalhadas_formatada2 = None
            saldo_horas = None

            if hora_inicio != '':
                
                hora_inicio_formatada = datetime.strptime(data + ' ' + hora_inicio, '%d/%m/%Y %H:%M')

            if hora_termino != '':
                
                hora_termino_formatada = datetime.strptime(data + ' ' + hora_termino, '%d/%m/%Y %H:%M')
            
            if horas_trabalhadas != '':

                horas_trabalhadas_formatada = datetime.strptime(data + ' ' + horas_trabalhadas, '%d/%m/%Y %H:%M')

            if hora_inicio_formatada is not None and hora_termino_formatada is not None and horas_trabalhadas_formatada is not None:
                hora_inicio_calculo = hora_inicio_formatada + timedelta(hours=1)
                horas_trabalhadas2 = hora_termino_formatada - timedelta(hours=hora_inicio_calculo.hour, minutes=hora_inicio_calculo.minute)

                if horas_trabalhadas_formatada > carga_horaria:

                    saldo_horas = horas_trabalhadas_formatada - timedelta(hours=carga_horaria.hour, minutes=carga_horaria.minute)

                else:

                    saldo_horas = carga_horaria - timedelta(hours=horas_trabalhadas_formatada.hour, minutes=horas_trabalhadas_formatada.minute)

            # if hora_termino != '' and horas_trabalhadas != '':
                # saldo_horas = horas_trabalhadas - carga_horaria

            # if hora_inicio != '':
            #     hora_inicio = 
            #     hInicio       = datetime.strptime(d + ' ' + horaInicio,'%d/%m/%Y %H:%M')

            # if horaTermino != '':
            #     hTermino      = datetime.strptime(d + ' ' + horaTermino,'%d/%m/%Y %H:%M')

            # if horasTrabalhadas != '':
            #     hTrabalhadas = datetime.strptime(d + ' ' + horasTrabalhadas,'%d/%m/%Y %H:%M')

            item = {
                    'ponto': ponto,
                    'codigo': int(horario[0].get_text('', strip=True)),
                    'dia_semana': horario[1].get_text('', strip=True),
                    'data': data_formatada,
                    'hora_inicio': hora_inicio_formatada,
                    'hora_termino': hora_termino_formatada,
                    'horas_trabalhadas': horas_trabalhadas_formatada,
                    'horas_trabalhadas2': horas_trabalhadas2,
                    'status': status,
                    'saldo': saldo_horas
            }

            client.estudo.horarios.update({'data': item['data']}, item, upsert=True)            

            # print(item)

            horarios.append(item)

        print('Dados extraidos com sucesso!')

    def export_csv(self):
        
        print('Inicio do import')

        client = MongoClient('mongodb://root:root@localhost:27017')        

        with open('assets/exports/horarios.csv', mode='w', encoding="utf-8", newline='') as csvfile:

            fieldnames = ['Codigo','DiaSemana','data','HoraInicio', 'HoraTermino', 'HorasTrabalhadas','Status']
            
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()

            for horario in client.estudo.horarios.find():

                data = datetime.strftime(horario['data'], '%d/%m/%Y')
                writer.writerow({'Codigo' : horario['codigo'], 'DiaSemana': horario['dia_semana'], 'data' : data, 'HoraInicio' : horario['hora_inicio'], 'HoraTermino' : horario['hora_termino'], 'HorasTrabalhadas' : horario['horas_trabalhadas'], 'Status': horario['status']})
    
        print('Export realizado com sucesso! Fim do export')

    def import_csv(self):

        print('Inicio do import')

        client = MongoClient('mongodb://root:root@localhost:27017')

        with open('assets/exports/horarios.csv', mode='r', encoding="utf-8", newline='') as csvfile:
            
            reader = csv.DictReader(csvfile)

            for row in reader:
                
                data             = datetime.strptime(row['data'], '%d/%m/%Y')
                hInicio          = datetime.strptime(row['HoraInicio'],'%Y-%m-%d %H:%M:%S')
                hTermino         = datetime.strptime(row['HoraTermino'],'%Y-%m-%d %H:%M:%S')
                hTrabalhadas     = datetime.strptime(row['HorasTrabalhadas'],'%Y-%m-%d %H:%M:%S')

                horario = {'codigo' : row['Codigo'], 'dia_semana': row['DiaSemana'], 'data' : data, 'hora_inicio' : hInicio, 'hora_termino' : hTermino, 'horas_trabalhadas' : hTrabalhadas, 'status' : row['Status']}
                
                client.estudo.horarios.update({'data': horario['data']}, horario, upsert=True)
        
        print('Importado realizado com sucesso! Fim do import')