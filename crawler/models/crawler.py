import requests
import json
import re 
import sys
from datetime import datetime, timedelta
import time
import csv
from bs4 import BeautifulSoup
from pymongo import MongoClient
from models.ponto import ponto

class crawler:

    def __init__(self):        
        """ """

        # self.url = 'http://ponto.grupoprazo.adv.br/Login/Autenticar/'
        self.url = 'http://localhost/login'
        # self.user = 'ncosta'
        # self.passwd = 'prazo@123'
        self.user = 'ncosta.rj@gmail.com'
        self.passwd = '123123'
        self.crawler_type = ''

    def set_type(self, crawler_type):

        self.crawler_type = crawler_type

    def test(self, html_doc):
        
        soup = BeautifulSoup(html_doc, 'lxml')

        return soup.find_all('input')[0]['value']


    def crawl(self):
        """ """

        if self.crawler_type == 'site':
            
            html_doc = self.crawl_website()

        elif self.crawler_type == 'html':

            html_doc = self.crawl_html()

        soup = BeautifulSoup(html_doc, 'lxml')

        self.extract_info(soup)

    def crawl_website(self):
        """ """

        # Start a session so we can have persistant cookies
        s = requests.session()
        
        # s = requests.session(config={'verbose': sys.stderr})

        r = s.get(self.url)

        token = self.test(html_doc = r.text)

        login_data = {
            # 'usuario': self.user,
            # 'senha': self.passwd  
            'email': self.user,
            'password': self.passwd,
            '_token': token
        }
        
        r = s.post(self.url, data=login_data, cookies=r.cookies)
        # r = s.get('http://ponto.grupoprazo.adv.br/Ponto/ExibirPonto')
        r = s.get('http://localhost/horarios')

        return r.text

    def crawl_html(self):
        """ """

        arquivo = open('/var/www/html/crawler/ponto.html', mode='r', encoding="utf-8")

        return arquivo.read()

    def extract_info(self, soup):
        
        """ Extraindo as informações do ponto """

        print('Início do robot extrator de dados do ponto')

        client         = MongoClient('mongodb://root:root@mongo:27017')

        tabela         = soup.find('div', class_="table-responsive")
        boxFuncionario = tabela.find('div', class_="jumbotron")        
        spans          = boxFuncionario.find_all('span')        
        boxPonto       = tabela.find('table', class_="table")

        dados = {
                'nome': spans[0].get_text('', strip=True), 
                'empresa': spans[1].get_text('', strip=True), 
                'inicio': spans[2].get_text('', strip=True), 
                'termino': spans[3].get_text('', strip=True), 
                'intervalo': spans[4].get_text('', strip=True),                
                }

        for linha in boxPonto.find_all('tr', class_="text-center"):

            reg = linha.find_all('td')

            # Dados
            codigo = reg[0].get_text('', strip=True)
            dia_da_semana = reg[1].get_text('', strip=True)
            data = reg[2].get_text('', strip=True)            
            hora_inicio = reg[3].get_text('', strip=True)
            hora_termino = reg[4].get_text('', strip=True)
            horas_computadas = reg[5].get_text('', strip=True)            
            status = reg[6].get_text('', strip=True)

            p = ponto(codigo=codigo, dia_da_semana=dia_da_semana, dados=dados, data=data, hora_inicio=hora_inicio, hora_termino=hora_termino, horas_computadas=horas_computadas, status=status)

            horario = {
                    'dados': p.dados,
                    'codigo': p.codigo,
                    'dia_semana': p.dia_da_semana,
                    'data': p.data,
                    'hora_inicio': p.hora_inicio,
                    'hora_termino': p.hora_termino,
                    'horas_computadas': p.horas_computadas,
                    'horas_trabalhadas': p.calcular_horas_trabalhadas(),
                    'status': p.status,
                    'saldo': p.calcular_saldo_horas()
            }

            client.estudo.horarios.update({'data': horario['data']}, horario, upsert=True)            

        print('Dados extraidos com sucesso!')

    def formatar_data(self, data):

        return  datetime.strptime(data, '%d/%m/%Y') 

    def formatar_data_hora(self, data, horario):

        return datetime.strptime(data + ' ' + horario, '%d/%m/%Y %H:%M')

    def export_csv(self):
        
        print('Inicio do export')

        client = MongoClient('mongodb://root:root@mongo:27017')        

        hoje = datetime.now()
        now = hoje.strftime('%Y%m%d')

        with open('/var/www/html/crawler/assets/exports/horarios_{now}.csv'.format(now=now), mode='w', encoding="utf-8", newline='') as csvfile:

            print('Exportando para csv!')

            fieldnames = ['Codigo','DiaSemana','data','HoraInicio', 'HoraTermino', 'HorasComputadas','Status']
            
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()

            dados = {
                        "inicio" : "09:00",
                        "nome" : "NEWTON GONZAGA COSTA",
                        "intervalo" : "1 hora",
                        "termino" : "18:48",
                        "empresa" : "ORION SERVICOS E LOGISTICA LTDA ME"
                    }      

            for horario in client.estudo.horarios.find():

                data = horario['data'].strftime('%d/%m/%Y')

                hora_inicio = ''
                hora_termino = ''
                horas_computadas = ''

                if horario['hora_inicio'] is not None:

                    hora_inicio = horario['hora_inicio'].strftime('%H:%M')

                if horario['hora_termino'] is not None:

                    hora_termino = horario['hora_termino'].strftime('%H:%M')

                if horario['horas_computadas'] is not None:

                    horas_computadas = horario['horas_computadas'].strftime('%H:%M')

                p = ponto(codigo=horario['codigo'], dia_da_semana=horario['dia_semana'], dados=dados, data=data, hora_inicio=hora_inicio, hora_termino=hora_termino, horas_computadas=horas_computadas, status=horario['status'])

                horas_trabalhadas = p.calcular_horas_trabalhadas()
                saldo = p.calcular_horas_trabalhadas()

                item = {
                        'Codigo' : p.codigo, 
                        'DiaSemana': p.dia_da_semana, 
                        'data' : data, 
                        'HoraInicio' : hora_inicio, 
                        'HoraTermino' : hora_termino,
                        'HorasComputadas' : horas_computadas,
                        'Status': p.status
                       }

                writer.writerow(item)
    
        print('Export realizado com sucesso!')

    def import_csv(self):

        print('Inicio do import')

        client = MongoClient('mongodb://root:root@mongo:27017')

        # hoje = datetime.now()
        now = datetime.strptime('20180901','%Y%m%d').date()
        now = now.strftime('%Y%m%d')
        

        with open('/var/www/html/crawler/assets/exports/horarios_{now}.csv'.format(now=now), mode='r', encoding="utf-8", newline='') as csvfile:
            
            print('Importando arquivo csv!')

            reader = csv.DictReader(csvfile)

            for row in reader:

                p = ponto(row['Codigo'], row['DiaSemana'], {}, row['data'], row['HoraInicio'], row['HoraTermino'], row['HorasComputadas'], row['Status'])

                codigo = row['Codigo']
                dados = {
                            "inicio" : "09:00",
                            "nome" : "NEWTON GONZAGA COSTA",
                            "intervalo" : "1 hora",
                            "termino" : "18:48",
                            "empresa" : "ORION SERVICOS E LOGISTICA LTDA ME"
                        }
                dia_da_semana = row['DiaSemana']
                data = row['data']
                hora_inicio = row['HoraInicio']
                hora_termino = row['HoraTermino']
                horas_computadas = row['HorasComputadas']
                status = row['Status']

                p = ponto(codigo=codigo, dia_da_semana=dia_da_semana, dados=dados, data=data, hora_inicio=hora_inicio, hora_termino=hora_termino, horas_computadas=horas_computadas, status=status)

                horario = {
                        'dados': p.dados,
                        'codigo': p.codigo,
                        'dia_semana': p.dia_da_semana,
                        'data': p.data,
                        'hora_inicio': p.hora_inicio,
                        'hora_termino': p.hora_termino,
                        'horas_computadas': p.horas_computadas,
                        'horas_trabalhadas': p.calcular_horas_trabalhadas(),
                        'status': p.status,
                        'saldo': p.calcular_saldo_horas()
                }
                
                client.estudo.horarios.update({'data': horario['data']}, horario, upsert=True)
        
        print('Fim do import')