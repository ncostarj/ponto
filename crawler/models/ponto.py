from datetime import datetime, timedelta
from pymongo import MongoClient

class ponto:

    def __init__(self, codigo, dia_da_semana, dados, data, hora_inicio, hora_termino, horas_computadas, status):
        """ """
        self.codigo = codigo
        self.dia_da_semana = dia_da_semana
        self.dados = dados
        self.data = self.formatar_data(data)
        self.carga_horaria = self.formatar_data_hora(data, ' 08:48')
        self.hora_inicio = self.formatar_data_hora(data, hora_inicio)
        self.hora_termino = self.formatar_data_hora(data, hora_termino)
        self.horas_computadas = self.formatar_data_hora(data, horas_computadas)
        self.status = status

    def __repr__(self):

        return "Point(codigo={codigo}, dia_da_semana={dia_da_semana}, dados={dados}, data={data}, carga_horaria={carga_horaria}, hora_inicio={hora_inicio}, hora_termino={hora_termino}, horas_computadas={horas_computadas}, status={status}\n".format(codigo=self.codigo, dia_da_semana=self.dia_da_semana, dados=self.dados, data=self.data, carga_horaria=self.carga_horaria, hora_inicio=self.hora_inicio, hora_termino=self.hora_termino, horas_computadas=self.horas_computadas, status=self.status)

    def calcular_horas_trabalhadas(self):

        # inicio calculo
        horas_trabalhadas = None
        hora_inicio_formatada = None
        hora_termino_formatada = None
        
        saldo_horas = None

        if self.hora_inicio is not None:
            
            hora_inicio_formatada = self.hora_inicio

        if self.hora_termino is not None:
            
            hora_termino_formatada = self.hora_termino

        if hora_inicio_formatada is not None and hora_termino_formatada is not None:
        #  and horas_computadas_formatada is not None:
            hora_inicio_calculo = hora_inicio_formatada + timedelta(hours=1)
            horas_trabalhadas = hora_termino_formatada - timedelta(hours=hora_inicio_calculo.hour, minutes=hora_inicio_calculo.minute)

        return horas_trabalhadas
    
    def calcular_saldo_horas(self):

        horas_computadas_formatada = None
        saldo_horas = None

        if self.horas_computadas is not None:

            horas_computadas_formatada = self.horas_computadas

        if horas_computadas_formatada is not None:

            if horas_computadas_formatada > self.carga_horaria:

                saldo_horas = horas_computadas_formatada - timedelta(hours=self.carga_horaria.hour, minutes=self.carga_horaria.minute)

            else:

                saldo_horas = self.carga_horaria - timedelta(hours=horas_computadas_formatada.hour, minutes=horas_computadas_formatada.minute)

        return saldo_horas

    def formatar_data(self, data):

        d = None

        if data != '':
            
            d = datetime.strptime(data, '%d/%m/%Y')

        return d

    def formatar_data_hora(self, data, hora):

        dh = None

        if data != '' and hora != '':

            dh = datetime.strptime(data + ' ' + hora, '%d/%m/%Y %H:%M')

        return dh